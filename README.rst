My vim config
#############

This project contains my personal vimrc. This file is largely taken from
spf13-vim_ vimrc, removing all stuff that I didn't need.

The configuration use the following modules:

- python-mode_
- vim-solarized_
- ...

.. contents::


Installation
============

To install this, just run the install.sh file.

.. _spf13-vim: http://vim.spf13.com/
.. _python-mode: https://github.com/klen/python-mode
.. _vim-solarized: http://ethanschoonover.com/solarized

