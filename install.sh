#!/usr/bin/env sh

url=https://bitbucket.org/Starch/vim-config.git
destination="$HOME/.vim-config"

endpath="$HOME/.vim-config"

info() {
    echo "INFO: $*"
}

warn() {
    echo "WARN: $*" >&2
}

die() {
    warn "$1"
    exit 1
}

backup_dir="$HOME/vim-backup-$(date +%Y%m%d)"
while [ -d "$backup_dir" ]; do
    backup_dir="$backup_dir.1"
done

info "Backing up current vim files into $backup_dir"
for i in $HOME/.vim $HOME/.vimrc $HOME/.gvimrc; do
    if [ -e $i ]; then
        if [ ! -L $i ]; then
            info "Backup file " $i
            mv "$i" "$backup_dir";
        else
            info "Removing link" $i
            rm "$i"
        fi
    fi
done

if [ ! -e "$endpath/.git" ]; then
    info "cloning vim-config"
    git clone --recursive $url $endpath
else
    info "updating vim-config"
    cd $endpath && git pull
fi


info "setting up symlinks"
ln -s $endpath/vimrc $HOME/.vimrc

mkdir -p $HOME/.vim/bundle
if [ ! -e $HOME/.vim/bundle/vundle ]; then
    info "Installing Vundle"
    git clone http://github.com/gmarik/vundle.git $HOME/.vim/bundle/vundle
fi

info "update/install plugins using Vundle"
system_shell=$SHELL
export SHELL="/bin/sh"
vim +BundleInstall! +BundleClean +qall
export SHELL=$system_shell

info "Installation complete"

